#!/bin/sh

#SBATCH --time=15:00:00
#SBATCH -p lyceum
#SBATCH --gres=gpu:1

module load conda/py3-latest
source activate jejenv

export LD_LIBRARY_PATH="$CONDA_PREFIX/lib"
export PATH="$PATH:$CONDA_PREFIX/bin"

export CUDA_PATH="$CONDA_PREFIX"
export CUDA_HOME="$CONDA_PREFIX"

python3 "train" -d "basic" -g "basic" -e 20 -m 5000  --checkpoint_directory "/scratch/jej1g19/checkpoints/" --checkpoint_rate 5 -i "/scratch/jej1g19/datasets/train.tfrecord" -go "/scratch/jej1g19/models/20_epoch_basic_spectrogram_model"