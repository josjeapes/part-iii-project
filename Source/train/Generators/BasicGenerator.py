from Generators import IGenerator

from tensorflow.keras import layers

import tensorflow as tf

class BasicGenerator(IGenerator):
    def __init__(self, model_description):

        self.model = self.BuildModel()

    def Postproccess(self, spectrogram):
        return tf.concat((tf.signal.inverse_stft(tf.cast(tf.squeeze(spectrogram), tf.complex64), frame_length=255, frame_step=128, window_fn=tf.signal.inverse_stft_window_fn(128),), [0]), axis=0)

    def Generate(self, count):

        noise = tf.random.normal([count, 100])

        labels = tf.random.uniform(
                        (count, 1), minval=0, maxval=11, dtype=tf.int32
                    )

        labels_encoded = tf.cast(tf.equal(tf.range(11)[tf.newaxis], labels), dtype=tf.float32)

        spectrograms = self.model(inputs=[noise, labels_encoded], training=True)

        audio = tf.map_fn(self.Postproccess, spectrograms)

        return {"audio": audio, "instrument_family": labels}

    def BuildModel(self):

        noise_inputs = tf.keras.Input(shape=(100,), name="noise")

        noise_features = layers.Dense(8 * 8 * 64)(noise_inputs)
        noise_features = layers.LeakyReLU()(noise_features)
        noise_features = layers.Reshape((8, 8, 64))(noise_features)

        label_inputs = tf.keras.Input(shape=(11,), name="label")

        label_features = layers.Dense(8 * 8 * 64)(label_inputs)
        label_features = layers.Reshape((8, 8, 64))(label_features)

        x = layers.concatenate([noise_features, label_features])

        x = layers.Conv2DTranspose(128, (5, 5), strides=(1, 1), padding="same", use_bias=False)(x)
        x = layers.BatchNormalization()(x)
        x = layers.LeakyReLU()(x)

        x = layers.Conv2DTranspose(64, (5, 5), strides=(2, 2), padding="same", use_bias=False)(x)
        x = layers.BatchNormalization()(x)
        x = layers.LeakyReLU()(x)

        x = layers.Conv2DTranspose(32, (5, 5), strides=(2, 2), padding="same", use_bias=False)(x)
        x = layers.BatchNormalization()(x)
        x = layers.LeakyReLU()(x)

        x = layers.Conv2DTranspose(16, (5, 5), strides=(2, 2), padding="same", use_bias=False)(x)
        x = layers.BatchNormalization()(x)
        x = layers.LeakyReLU()(x)

        x = layers.Conv2DTranspose(
            1,
            (5, 5),
            strides=(2, 2),
            padding="same",
            use_bias=False,
            activation="tanh",
        )(x)


        output = layers.Resizing(499, 129)(x)

        model = tf.keras.Model(
            inputs=[noise_inputs, label_inputs], outputs=[output], name="Generator"
        )


        model.summary()

        return model
