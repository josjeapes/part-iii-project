from abc import ABC, abstractmethod


class IGenerator(ABC):
    @abstractmethod
    def __init__(self, model_description):
        pass

    @abstractmethod
    def Generate(self, count):
        pass


class TestGenerator(IGenerator):
    def __init__(self, model_description):
        print(model_description.instrument_families)

    def Generate(self, noise, label):
        return []
