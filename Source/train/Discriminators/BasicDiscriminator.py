from email.mime import audio
from Discriminators import IDiscriminator

from tensorflow.keras import layers

import tensorflow as tf

class BasicDiscriminator(IDiscriminator):
    def __init__(self, model_description):

        self.model = self.BuildModel()

    def Parse(self, data):
        features = {
            "instrument_family": tf.io.FixedLenFeature([], tf.int64),
            "audio": tf.io.FixedLenSequenceFeature([], tf.float32, allow_missing=True),
        }

        return tf.io.parse_single_example(data, features)

    def Preproccess(self, data):
        spectrogram = tf.abs(tf.signal.stft(data["audio"], frame_length=255, frame_step=128))
        return {"instrument_family": data["instrument_family"], "spectrogram": spectrogram[..., tf.newaxis]}

    def PreproccessShape(self):
        return {"instrument_family":tf.int32, "spectrogram":tf.float32}

    def BuildModel(self):

        spectrogram_inputs = tf.keras.Input(shape=(499, 129, 1), name="spectrogram")

        spectrogram_features = layers.Resizing(128, 128)(spectrogram_inputs)

        spectrogram_features = layers.Conv2D(16, (5, 5), strides=(2, 2), padding="same")(spectrogram_features)
        spectrogram_features = layers.LeakyReLU()(spectrogram_features)
        spectrogram_features = layers.Dropout(0.3)(spectrogram_features)

        spectrogram_features = layers.Conv2D(32, (5, 5), strides=(2, 2), padding="same")(spectrogram_features)
        spectrogram_features = layers.LeakyReLU()(spectrogram_features)
        spectrogram_features = layers.Dropout(0.3)(spectrogram_features)

        label_inputs = tf.keras.Input(shape=(1,), dtype=tf.int32, name="label")

        label_features = layers.Embedding(11, 100)(label_inputs)
        label_features = layers.Dense(32 * 32 * 1)(label_features)
        label_features = layers.Reshape((32, 32, 1))(label_features)

        x = layers.concatenate([spectrogram_features, label_features])

        x = layers.Conv2D(64, (5, 5), strides=(2, 2), padding="same")(x)
        x = layers.LeakyReLU()(x)
        x = layers.Dropout(0.3)(x)

        x = layers.Conv2D(128, (5, 5), strides=(2, 2), padding="same")(x)
        x = layers.LeakyReLU()(x)
        x = layers.Dropout(0.3)(x)

        x = layers.Flatten()(x)

        output = layers.Dense(1)(x)

        model = tf.keras.Model(
            inputs=[spectrogram_inputs, label_inputs], outputs=[output], name="Discriminator"
        )

        model.summary()

        return model

    def Prediction(self, data):
        return self.model(inputs=[data["spectrogram"], data["instrument_family"]], training=True)
