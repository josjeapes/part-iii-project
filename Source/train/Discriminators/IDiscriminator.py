from abc import ABC, abstractmethod


class IDiscriminator(ABC):
    @abstractmethod
    def __init__(self, model_description):
        pass

    @abstractmethod
    def Parse(self, data):
        pass

    @abstractmethod
    def Preproccess(self, data):
        pass

    @abstractmethod
    def PreproccessShape(self):
        pass

    @abstractmethod
    def Prediction(self, data):
        pass


class TestDiscriminator(IDiscriminator):
    def __init__(self, model_description):
        print(model_description.instrument_families)

    def Prediction(self, data):
        return data
