from email import generator
import tensorflow as tf

import time

import os


class Model:
    def __init__(
        self,
        model_description,
        checkpoint_directory,
        checkpoint_rate,
        checkpoint_continue,
        generator_output,
    ):
        self.model_description = model_description

        self.generator = model_description.generator(model_description)
        self.discriminator = model_description.discriminator(model_description)

        self.generator_optimizer = tf.keras.optimizers.Adam(
            model_description.learning_rate
        )

        self.discriminator_optimizer = tf.keras.optimizers.Adam(
            model_description.learning_rate
        )

        self.batch_size_per_replica = model_description.batch_size
        self.global_batch_size = self.batch_size_per_replica * 1

        self.shuffle_buffer = model_description.shuffle_buffer

        self.labels = model_description.labels

        self.seed = model_description.seed

        if checkpoint_directory is not None:

            self.checkpoint_directory = checkpoint_directory
            self.checkpoint_rate = checkpoint_rate
            self.checkpoint_prefix = os.path.join(checkpoint_directory, "ckpt")
            self.checkpoint = tf.train.Checkpoint(
                generator_optimizer=self.generator_optimizer,
                discriminator_optimizer=self.discriminator_optimizer,
                generator=self.generator.model,
                discriminator=self.discriminator.model,
            )

            if checkpoint_continue:
                self.checkpoint.restore(
                    tf.train.latest_checkpoint(checkpoint_directory)
                )
        else:
            self.checkpoint_directory = None

        if generator_output is not None:
            self.generator_output = generator_output
        else:
            self.generator_output = generator_output

    @tf.function
    def random_labels(self, labels, count):
        output = tf.zeros([count, 0], dtype=tf.int32)

        for label_type in labels:
            output = tf.concat(
                [
                    output,
                    tf.random.uniform(
                        (count, 1), minval=0, maxval=len(label_type), dtype=tf.int32
                    ),
                ],
                axis=1,
            )
        return output

    @tf.function
    def discriminator_loss(self, real_output, fake_output):
        real_per_example_loss = tf.keras.losses.BinaryCrossentropy(from_logits=True)(
            tf.ones_like(real_output), real_output
        )

        fake_per_example_loss = tf.keras.losses.BinaryCrossentropy(from_logits=True)(
            tf.zeros_like(fake_output), fake_output
        )

        per_example_loss = real_per_example_loss + fake_per_example_loss

        return per_example_loss

    @tf.function
    def generator_loss(self, fake_output):
        per_example_loss = tf.keras.losses.BinaryCrossentropy(from_logits=True)(
            tf.ones_like(fake_output), fake_output
        )

        return per_example_loss

    @tf.function
    def train_step(self, real_data):

        with tf.GradientTape() as generator_tape, tf.GradientTape() as discriminator_tape:

            fake_data = self.generator.Generate(count = self.batch_size_per_replica)

            fake_data = tf.map_fn(self.discriminator.Preproccess, fake_data, fn_output_signature=self.discriminator.PreproccessShape())

            real_output = self.discriminator.Prediction(real_data)
            fake_output = self.discriminator.Prediction(fake_data)

            generator_loss = self.generator_loss(fake_output)
            dsicriminator_loss = self.discriminator_loss(real_output, fake_output)

        gradients_of_generator = generator_tape.gradient(
            generator_loss, self.generator.model.trainable_variables
        )
        gradients_of_discriminator = discriminator_tape.gradient(
            dsicriminator_loss, self.discriminator.model.trainable_variables
        )

        self.generator_optimizer.apply_gradients(
            zip(gradients_of_generator, self.generator.model.trainable_variables)
        )
        self.discriminator_optimizer.apply_gradients(
            zip(
                gradients_of_discriminator,
                self.discriminator.model.trainable_variables,
            )
        )

    def train(self, dataset, epochs):

        dataset = dataset.map(self.discriminator.Parse)
        dataset = dataset.map(self.discriminator.Preproccess)
        dataset = dataset.shuffle(self.shuffle_buffer, seed=self.seed)
        dataset = dataset.batch(self.global_batch_size)

        for epoch in range(epochs):

            start = time.time()

            for step, data in enumerate(dataset):
                self.train_step(data)

            print("Time for epoch {} is {} sec".format(epoch + 1, time.time() - start))

            if self.checkpoint_directory is not None:
                if (int(epoch) + 1) % self.checkpoint_rate == 0:
                    self.checkpoint.save(file_prefix=self.checkpoint_prefix)

        if self.generator_output is not None:
            self.generator.model.save(self.generator_output)
