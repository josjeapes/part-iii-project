from ModelDescription import *
from Model import *

import time
import tensorflow as tf

from Generators import GetGenerator
from Discriminators import GetDiscriminator

import matplotlib.pyplot as plt

import argparse

parser = argparse.ArgumentParser()

parser.add_argument(
    "--discriminator",
    "-d",
    help="name of the discriminator model to use",
    required=True,
)

parser.add_argument(
    "--generator", "-g", help="name of the generator model to use", required=True
)

parser.add_argument(
    "--learning_rate",
    "-l",
    help="model learning rate, defaults to 1e-4",
    default=1e-4,
    type=float,
)

parser.add_argument(
    "--batch_size", "-b", help="batch size, defaults to 256", default=256, type=int
)

parser.add_argument(
    "--buffer_size",
    "-m",
    help="size of the buffer for shuffling the training data, defaults to 289206",
    default=289206,
    type=int,
)

parser.add_argument(
    "--seed", "-s", help="The random seed to use, defaults to 10", default=10, type=int
)

parser.add_argument(
    "--epochs", "-e", help="the number of epochs to process", required=True, type=int
)

parser.add_argument(
    "--checkpoint_directory",
    help="folder to store model checkpoints, leave empty to not save checkpoints",
)

parser.add_argument(
    "--checkpoint_rate",
    help="how many epochs between checkpoints, defaults to 20",
    default=20,
    type=int,
)

parser.add_argument(
    "--checkpoint_continue",
    dest="checkpoint_continue",
    action="store_true",
    help="should continue training from the latest checkpoint in the provided checkpoint directory",
)

parser.add_argument(
    "--generator_output",
    "-go",
    help="where to output the trained generator model, to be loaded used seperated from this program",
)

parser.add_argument(
    "--dataset",
    "-i",
    help="file path to the training dataset",
    required=True,
)

parser.set_defaults(checkpoint_continue=False)

args = parser.parse_args()

if args.checkpoint_continue and (args.checkpoint_directory is None):
    parser.error("--checkpoint_continue requires --checkpoint_directory.")

#

tf.random.set_seed(args.seed)


families = [
    [
        "bass",
        "brass",
        "flute",
        "guitar",
        "keyboard",
        "mallet",
        "organ",
        "reed",
        "string",
        "synth lead",
        "vocal",
    ]
]

dataset = tf.data.TFRecordDataset(args.dataset)


model_description = ModelDescription(
    families,
    generator=GetGenerator(args.generator),
    discriminator=GetDiscriminator(args.discriminator),
    learning_rate=args.learning_rate,
    batch_size=args.batch_size,
    shuffle_buffer=args.buffer_size,
    seed=args.seed
)

model = Model(
    model_description,
    args.checkpoint_directory,
    args.checkpoint_rate,
    args.checkpoint_continue,
    args.generator_output,
)

start = time.time()

model.train(dataset, args.epochs)

print("done training in {}".format(time.time() - start))

plt.show()
