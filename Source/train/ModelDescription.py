class ModelDescription:
    def __init__(
        self,
        labels,
        generator,
        discriminator,
        learning_rate,
        batch_size,
        shuffle_buffer,
        seed,
    ):
        self.labels = labels
        self.generator = generator
        self.discriminator = discriminator
        self.learning_rate = learning_rate
        self.batch_size = batch_size
        self.shuffle_buffer = shuffle_buffer
        self.seed = seed
