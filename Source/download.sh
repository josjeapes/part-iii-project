#!/bin/sh

module load conda/py3-latest
source activate jejenv

export LD_LIBRARY_PATH="$CONDA_PREFIX/lib"
export PATH="$PATH:$CONDA_PREFIX/bin"

export CUDA_PATH="$CONDA_PREFIX"
export CUDA_HOME="$CONDA_PREFIX"

python3 "download" -o /scratch/jej1g19/datasets