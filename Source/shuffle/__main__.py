import argparse

import tensorflow as tf

parser = argparse.ArgumentParser()

parser.add_argument("--input", "-i", help="the dataset to shuffle", required=True)

parser.add_argument(
    "--output", "-o", help="the target output for the shuffled dataset", required=True
)

parser.add_argument(
    "--seed",
    "-s",
    help="the random seed to use, defaults to 10",
    default=10,
    type=int,
)

args = parser.parse_args()

dataset = tf.data.TFRecordDataset(args.input)

dataset.shuffle(289205, seed=args.seed)

with tf.io.TFRecordWriter(args.output) as w:
    for record in dataset:
        w.write(record)
