import tensorflow as tf

import argparse

import matplotlib.pyplot as plt

import time

from scipy.io.wavfile import write

import numpy as np

parser = argparse.ArgumentParser()


parser.add_argument(
    "--model",
    "-m",
    help="the generator model to use",
    required=True,
)

parser.add_argument(
    "--noise",
    "-n",
    help="the number of noise inputs, must match the model, defaults to 100",
    default=100,
    type=int,
)

parser.add_argument(
    "--label",
    "-l",
    help="the class label to use, if not provided uses random label",
    type=int,
)

parser.add_argument(
    "--count",
    "-c",
    help="the number of outputs to generate, defaults to 1",
    default=1,
    type=int,
)

parser.add_argument(
    "--output", "-o", help="the directory in which to store the output", required=True
)

args = parser.parse_args()

tf.random.set_seed(time.time_ns())

model = tf.keras.models.load_model(args.model, compile=False)

noise = tf.random.normal([args.count, args.noise])

if args.label is None:
    labels = tf.random.uniform((args.count, 1), minval=0, maxval=10, dtype=tf.int32)
else:
    labels = tf.fill((args.count, 1), args.label)

def Postproccess(spectrogram):
    return tf.concat((tf.signal.inverse_stft(tf.cast(tf.squeeze(spectrogram), tf.complex64), frame_length=255, frame_step=128, window_fn=tf.signal.inverse_stft_window_fn(128),), [0]), axis=0)

labels_encoded = tf.cast(tf.equal(tf.range(11)[tf.newaxis], labels), dtype=tf.float32)

output = model(inputs=[noise, labels_encoded])

audio = tf.map_fn(Postproccess, output)

for i in range(args.count):
    write(
        "./{}/{:04d}_{}.wav".format(args.output, i, labels[i]),
        16000,
        audio[i].numpy().astype(np.float),
    )

# output = output * 127.5 + 127.5

# images = tf.concat([output, output, output], axis=3)

# for i in range(args.count):
#     image = tf.keras.preprocessing.image.array_to_img(images[i, :, :, :])
#     image.save("{}/{:04d}_{}.png".format(args.output, i, labels[i]))
