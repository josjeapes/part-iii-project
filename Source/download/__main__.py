import argparse
import os

import shutil
import requests

parser = argparse.ArgumentParser()

parser.add_argument(
    "--output", "-o", help="the target folder for the dataset download", required=True
)

args = parser.parse_args()


def download_dataset(name, output_folder):
    file = output_folder + "/" + name + ".tfrecord"

    if not os.path.isfile(file):
        url = (
            "http://download.magenta.tensorflow.org/datasets/nsynth/nsynth-"
            + name
            + ".tfrecord"
        )

        with requests.get(url, stream=True) as r:
            with open(file, "wb") as f:
                shutil.copyfileobj(r.raw, f)


download_dataset("test", args.output)
download_dataset("valid", args.output)
download_dataset("train", args.output)
