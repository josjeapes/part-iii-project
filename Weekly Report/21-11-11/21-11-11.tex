\section{November 11, 2021}



\subsection{GAN Basics}

\glspl{gan} are a form of generative model\cite{goodfellow2017nips}.  A generative model aims to take a training set and then create an output with the maximum chance of belonging to that set. Example use cases of generative models include upscaling images and predicting the next frame of a video - any task that requires more information than was originally provided will need generative modelling of some kind. The current methods of synthesizing musical samples are therefore already generative models; interpolation, for example, takes the two nearest available note samples and provides an output that is between them.

\glspl{gan} avoid a problem that happens in \gls{ml} generative models that are trained to reduce mean squared error where they output the average of the samples from the training set rather than a single sample with a high probability of fitting the set. When upscaling an image, this is the difference between creating a blurred version of all possible correct upscaled images instead of creating one of the possible upscaled images. \cite{DBLP:journals/corr/LedigTHCATTWS16} Different guitars have different tones, and if a generative model was trained with a mean squared error function it would output the average of all these tones, which might sound muddy or at least very generic, rather than being able to output guitar samples with a range of tones.

The core idea behind them is that you take two separate models (they do not need to be neural networks, but typically are). One is called the generator, the other is called the discriminator. The generator is fed random noise and generates a piece of data (an image, or for my project an audio sample). The discriminator takes data as an input and outputs a single number - a prediction of whether or not the data belongs to the training set or not. As you feed training set data and generated data to the discriminator, it gets better at differentiating the two - which allows you to train the generator to create pieces of data that better fit the training set. \cite{AGGARWAL2021100004}

\subsubsection{Mathematical Definition}

For a training set \( \mathcal{D} = \{\mathbf{x_n}\}, n = 1,...,N\), the aim is to create a generator \( G(\mathbf{z};\mathbf{\omega_G}) \) that generates data with the maximum likelihood  of having all the properties common to the data in \( \mathcal{D} \). This model can be seen in figure \ref{fig:gan block diagram}. The discriminator \( D(.;\mathbf{\omega_D}) \) takes an input of either \( \mathbf{x_n} \) or \( G(\mathbf{z}) \) and outputs a guess as to whether the input belongs to \( \mathcal{D} \) or not.

The discriminator tries to make the following two statements true:

\[ D(\mathbf{x_n}) = 1 \]
\[ D(G(\mathbf{z})) = 0 \]

While the generator tries to make the following true:

\[ D(G(\mathbf{z})) = 1 \]



So long as the models \( D(.;\mathbf{\omega_D}) \) and \( G(.;\mathbf{\omega_G}) \) are differentiable, they can be trained by gradient descent just like a normal \gls{ml} model. The strength and the name of \glspl{gan} come from this competition over the value of \( D(G(\mathbf{z})) \). If the discriminator was present with equal amounts of both input types, eventually the value would tend towards \( D(G(\mathbf{z})) = 0.5\) as it becomes impossible to tell apart the generated data from the training set.

\begin{figure}[H]
  \centering
  \begin{tikzpicture}

    \node[draw,
    circle,
    minimum size = 0.6cm,
    ] (or) at (0,0){$\mathbf{or}$};

    \node[draw,
    minimum width = 2cm,
    minimum height = 1.2cm,
    below = 1cm of or
    ] (generator) {$G()$};

    \node[draw,
    minimum width = 2cm,
    minimum height = 1.2cm,
    right=1cm of or,
    ] (discriminator) {$D()$};

    \draw[stealth-] (generator.west) -- ++(-3,0)
      node[midway, above]{random noise $\mathbf{z}$};

    \draw[stealth-] (or.west) -- ++(-3.5,0)
      node[midway, above]{training data $\mathbf{x}\in\mathcal{D}$};

    \draw[-stealth] (generator.north) -- (or.south)
      node[midway,xshift=-1.7cm]{generated data $\mathbf{z}$};

    \draw[stealth-] (discriminator.west) -- (or.east)
      node[midway,above]{$\mathbf{d}$};

    \draw[-stealth] (discriminator.east) -- ++(3,0)
      node[midway, above]{$P(\mathbf{d}\in\mathcal{D})$};

  \end{tikzpicture}

  \caption{basic GAN block diagram}
  \label{fig:gan block diagram}
\end{figure}

\subsubsection{CGANs}

If the above process was followed for the training set I currently plan on using, which includes a multitude of samples with different instruments and pitches, then the generator would actually be trained to create an audio sample that sounds like a random instrument at a random pitch, there would be no way to control it (actually, it would probably settle on just one instrument due to \hyperref[mode collapse]{Mode Collapse}).

\begin{figure}[H]
  \centering
  \begin{tikzpicture}

    \node[draw,
    circle,
    minimum size = 0.6cm,
    ] (or) at (0,0){$\mathbf{or}$};

    \node[draw,
    minimum width = 2cm,
    minimum height = 1.2cm,
    below = 1cm of or
    ] (generator) {$G()$};

    \node[draw,
    minimum width = 2cm,
    minimum height = 1.2cm,
    right=1cm of or,
    ] (discriminator) {$D()$};

    \draw[stealth-] (generator.west) -- ++(-3,0)
      node[midway, above]{random noise $\mathbf{z}$};

    \draw[stealth-] (or.west) -- ++(-3.5,0)
      node[midway, above]{training data $\mathbf{x}\in\mathcal{D}$};

    \draw[-stealth] (generator.north) -- (or.south)
      node[midway,xshift=-1.7cm]{generated data $\mathbf{z}$};

    \draw[stealth-] (discriminator.west) -- (or.east)
      node[midway,above]{$\mathbf{d}$};

    \draw[-stealth] (discriminator.east) -- ++(3,0)
      node[midway, above]{$P(\{\mathbf{d},\mathbf{y}\}\in\mathcal{D})$};

      \draw[stealth-] (generator.east) -- ++(1.4,0)
      node[midway, right]{};

    \draw[stealth-] (discriminator.south) -- ++(0,-3)
    node[midway, right]{training label $\mathbf{y}\in\mathcal{D}$};

  \end{tikzpicture}

  \caption{CGAN block diagram}
  \label{fig:cgan block diagram}
\end{figure}

\glspl{cgan} are a version of \gls{gan} that operate on a labelled training set \( \mathcal{D} = \{\mathbf{x_n, y_n}\}, n = 1,...,N\) where the labels are also passed into both the generator and the discriminator so the discriminator's prediction of data belonging to the training set is now \( D(\mathbf{x_n}^\frown\mathbf{y_n}) \) or \( D(G(\mathbf{z}^\frown\mathbf{y})) \). This allows the discriminator to change the output based on what the \gls{gan} is supposed to be creating, allowing us to train a configurable generator off of labelled data. \cite{9034368} A \gls{cgan} model can be seen in figure \ref{fig:cgan block diagram}

Because of the nature of the project, the \glspl{gan} I implement will all be \glspl{cgan}.

\subsection{Potential Problems}

A major problem faced by \glspl{gan} is non-convergence. Optimization algorithms used by other \gls{ml} systems will typically converge on a local minimum error when using gradient descent but because of the game-like, competitive nature of \glspl{cgan}, there is potential for them to never settle on an optimal solution because as one model gains ground the other can lose it. In practice, they often end up oscillating back and forth \cite{DBLP:journals/corr/KodaliAHK17} but this is a current area of research and I cannot find a way to predict when it will happen.

\subsubsection{Mode Collapse}
\label{mode collapse}

Mode collapse is a common and problematic form of non-convergence in \glspl{gan}. If the data set is made up of separate clumps of data with large gaps between them (say, different instruments with vastly different timbres) then the generator can learn to simply generate results that are very close to a single one of these modes. Eventually, the discriminator will learn to just reject all inputs belonging to that mode, at which point the generator will switch to a different mode. No matter how long you train the system for, it will keep going round in circles and never reach the point where the generator will create data that could belong to any of the modes \cite{9312049}. There are a number of different ways to tackle this problem if it emerges, which often involve comparing different generator outputs to check how similar they are.

The most obvious modes my system could collapse into are the different instruments, but the fact that I am using a \gls{cgan} should help counteract that as I am telling the generator which mode to create, and the discriminator which to expect.

\subsubsection{Standard Pitch Recognition}

The training set will be made up exclusively of standard pitch samples and not of microtones. If, as part of the training, the generator is told to generate microtones the discriminator will learn to call out anything the generator creates that isn't standard pitch, teaching the generator to only generate standard pitch samples which is not ideal because the point of the project is to be able to generate audio samples with the same timbre but a different pitch than those you have samples for.

\gls{gan} generator output changes smoothly as you change the input, however, and the result of this is that  by training the model just on standard pitches, it will develop an understanding of the 'essence' of the timbre of the samples, and then be able to interpolate between them. \cite{radford2016unsupervised}

\subsection{Assessment of the Generated Samples}
\label{assessment of the generated samples}

Although timbre is ultimately a physical property, it is so complicated that assessing its accuracy can't be done objectively.

My current plan for how to assess the quality of the samples produced by my system includes the following methods:

\begin{enumerate}
  \item \textbf{Sense Check} - Taking a random set of generated samples and making a qualitative check that they sound like the instrument they are supposed to. This can't be used to assess whether the system is generating high-quality audio samples, but if my samples fail this check then it can't possibly be  working as desired.
  \item \textbf{\gls{ml} Test}  - Using a completely separate classification model that takes an input sample and outputs an instrument label as well as a completely separate regression model that takes an input sample and outputs a pitch on the generated samples is a more quantitive check of quality. The values these models generate can then be compared to the labels the generator was fed. The pass rate (or the confidence for instrument and error for pitch) could be compared to that of samples generated by existing methods, allowing a comparison between my model and other methods. Using a completely separate data set for the training is ideal, but there will still be value in this test if one cannot be found because it will demonstrate that the generator is also solving the problem, rather than just fooling the discriminator.
  \item \textbf{Correlation Test} - A repeatable test I have designed that attempts to quantify the success of different sample generation methods.
  \begin{enumerate}
    \item Retrain model in exactly the same way, with samples for a single pitch and instrument removed from the training data (for example, piano C4).
    \item Generate a sample for the pitch and instrument that were removed using the model trained in (a) and a control method to be compared against.
    \item Perform cross-correlation, or a similar comparison operation, of the generated samples with the removed sample(s)
    \item The model whose generated sample has the highest peak cross-correlation is the one that is generating samples with the most accurate timbre.
 \end{enumerate}
 This method avoids the completely separate data set requirement of the \gls{ml} test, and also is arguably a better test of the precise goal of the project - replicating a specific timbre at a pitch not included in the training set.
  \item \textbf{Blind Trial} - If the \gls{ml} test and correlation test both fail to reasonably demonstrate a difference between my results and the current methods, it may be necessary to use a blind trial. A possibility is asking participants which of two different scales - one including a sample generated by my system, one by a current method - and asking which sounds better. A major problem with this is that people's opinions are subjective, but timbre is a physical property - so in theory the correlation test is more useful. Some of the subjectivity could be removed by only asking music students as they are more likely to have a trained ear.
\end{enumerate}