\section{10th March, 2022}

\subsection{Dataset Filtering}

I've previously discussed how unwieldy the dataset is in its current form, so this week took the steps needed to fix this.

For the sake of consistency with GANSynth, I filtered out pitches above C6 and below C1. This trimmed the dataset from 289,295 examples down to 223,775.

Trying to generate both electric and acoustic sounds is possible but an increase in problem complexity, so the examples with electric sources have been removed. It also doesn't make sense to use synthesized audio when training a model to generate audio that sounds as natural as possible. This leaves just the acoustic sample, trimming the dataset down to a significantly more usable size of 81,454.

At this point, it is worth looking at the number of examples per instrument family, which can be seen in table \ref{table instrument family count}.

\begin{table}[H]
  \centering
  \begin{tabular}{|c|r|}
    \hline
    Family     & Example Count \\
    \hline
    \hline
    Bass       & 200           \\
    \hline
    Brass      & 11985         \\
    \hline
    Flute      & 4207          \\
    \hline
    Guitar     & 8350          \\
    \hline
    Keyboard   & 5676          \\
    \hline
    Mallet     & 18735         \\
    \hline
    Organ      & 158           \\
    \hline
    Reed       & 11333         \\
    \hline
    String     & 16990         \\
    \hline
    Synth Lead & 0             \\
    \hline
    Vocal      & 3820          \\
    \hline
  \end{tabular}
  \caption{Instrument family example count after filtering out pitches and non-acoustic sources.}
  \label{table instrument family count}
\end{table}

Note that the low sample count for some of the families comes from the fact that most of their examples are electronic or synthesized - for example, most of the datasets' organ examples are synthesized. It makes a lot of sense to trim these low-count families (bass, organ, synth lead, and vocal) leaving a grand total of 77,276 pieces of data.

In this way, we have achieved an almost 75\% reduction in the amount of data without any real reduction in the likely output quality.

I shard the dataset (each shard includes the examples from the dataset where the example index mod the shard count equals the shard number). Each shard is 512 examples long. Sharding helps mix up the data as the instrument families are grouped contiguously. and I then shuffle the shards individually so that the first example from each shard isn't the same family.

This has a number of advantages with regard to running the model - the data doesn't need to be shuffled at all, and shuffling takes a long time. The order in which the shards are used can still be shuffled, preventing every epoch being exactly the same. The separate GPUs can also work on separate shards - granting a slight speed increase.

It actually takes about three hours to create one of these shards - so they can't all be created at once. Instead, what I have done is create a job for each shard - allowing them to all be queued simultaneously and get past the time limit. This is currently running.

\subsection{What is left to do before running}

I've implemented a majority of the changes mentioned last week, and the only remaining steps before running are as follows:

\begin{itemize}
  \item The statistic gathering and metric implementation. If the metrics can run on the finished models - which most of them can - this can actually be delayed until after training starts.
  \item Generator model output at different stages - this is very simple to implement, just needs to be written.
  \item Tensorboard callback log implementation - to record performance and check that the system is working as I intended. This is standard practice and shouldn't take long, though I haven't done it before.
  \item Change the input code to handle the sharded tfrecord files, which looks like it will be just a small change to the loading function.
  \item Implement checkpoints and set things up so that I can get around the time limit. The stage changes would be a natural way to do this.
\end{itemize}

Note that these are all very small changes that I just haven't got round to implementing yet. I'd be very surprised if the model isn't in the middle of running next week.

There is one larger change I could make - scaling the weights based on the He scaling factor at runtime, rather than at initialization. This is what GANSynth does, and it is a method that is used to prevent weights exploding in value as you train. However, it requires reimplementing the standard layers (dense, Conv2D) with the scaling used. I have written other custom layers for the model, but none that use weights - and while the implementation should be relatively simple there is potential for things to go wrong. My decision is therefore that, while this model is running I will implement my model. Then, if this model produces poor results it has to be because of the lack of this equalization  - and I will know to implement equalization before running again. It is important to get this right so that I give myself enough report writing time.