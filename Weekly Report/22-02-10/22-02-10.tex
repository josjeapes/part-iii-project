
\section{Febuary 10th, 2022}

\subsection{CGAN Implementation}

Over the past couple of weeks, I've been programming the actual \gls{gan}. I've written an interface class for both the discriminator and generator and the training is done on those interfaces. This will make it extremely simple to swap out, and therefore compare, different models. The interfaces are also not specific to the problem (in that they don't ask for instrument families and audio data, but rather labels and generic training data) but could be used for any \gls{cgan}.

In my progress report, I said that the generator would create spectrograms, but it makes more sense for the generator to create output in the exact format that is required (raw audio data). Processing like converting to and from spectrograms should happen inside the generator and discriminator. This generalisation also allows me to test my setup on a simpler problem - generating handwritten digits from the MNIST dataset \cite{deng2012mnist}.

Some examples of MNIST data is included in figure \ref{fig:mnist example}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.3\textwidth]{22-02-10/mnist example.png}
  \caption{MNIST dataset examples}
  \label{fig:mnist example}
\end{figure}

\subsubsection{First Model}

Below is the generator for the first model I implemented:

\lstinputlisting{22-02-10/First Model/BasicGeneratorModel.txt}

Below is the discriminator for the first model I implemented:

\lstinputlisting{22-02-10/First Model/BasicDiscriminatorModel.txt}

Both are built on 2D convolution layers which, as we have been seen before, convolute a trainable kernel over the input data as if it were an image. Both take a single number (the digit to draw, detect) as the label and convert it to an array of ten 0s, with a 1 in the index equal to the digit value. The generator then simply concatenates this with the noise input, whereas the discriminator concatenates it with the processed input before the final dense layer.

Training this model for 100 epochs on batches of 256 real images and 256 generated images took three and a half hours on my computer, without GPU acceleration.

\begin{figure}[H]
  \centering

  \begin{subfigure}[b]{0.3\textwidth}
    \includegraphics[width=\textwidth]{22-02-10/First Model/image_at_epoch_0001.png}
    \caption{Epoch 1}
    \label{fig:first cgan output 1}
  \end{subfigure}%
  \begin{subfigure}[b]{0.3\textwidth}
    \includegraphics[width=\textwidth]{22-02-10/First Model/image_at_epoch_0002.png}
    \caption{Epoch 2}
    \label{fig:first cgan output 2}
  \end{subfigure}

  \begin{subfigure}[b]{0.3\textwidth}
    \includegraphics[width=\textwidth]{22-02-10/First Model/image_at_epoch_0010.png}
    \caption{Epoch 10}
    \label{fig:first cgan output 10}
  \end{subfigure}%
  \begin{subfigure}[b]{0.3\textwidth}
    \includegraphics[width=\textwidth]{22-02-10/First Model/image_at_epoch_0050.png}
    \caption{Epoch 50}
    \label{fig:first cgan output 50}
  \end{subfigure}

  \caption{Example output for labels 1-9, after different training epochs for the first model}
  \label{fig:first cgan output}

\end{figure}

The square-like artefacts that appear in figure \ref{fig:first cgan output 1} is a result of the noise and convolutional layer kernels. After ten epochs, in figure \ref{fig:first cgan output 10}, the generator is creating images that start to look like they have pen strokes - rough lines of the same thickness as the dataset. However, the model settles as about epoch 50 into the shapes seen in figure \ref{fig:first cgan output 50}. These resemble handwritten digits (the three looks like a six, the five looks like a nine), but certainly not the spicefic digit that they are supposed to represent. There is so little change in output that it is unlikely this model would get better if trained for longer. Arguably, it is behaving like a normal \gls{gan} rather than a \gls{cgan} in that the labels are being ignored. This prompted me to try a different model.

\subsubsection{Second Model}

After some research, I came up with a different model (seen below) which uses an embedding layer on the label for both inputs \cite{chollet2015keras}. This maps a class label to some number of outputs, using trainable weights. These layers are often used in natural language processing, where there are a great number of classes (each word is typically its own class). Embedding layers will definitely be applicable to the discriminator in my final model due to the large number of pitch classes. An example of how such a layer can be effective is if you were trying to generate output that looked like LCD digits. You could train a system that translates the digit to be displayed into whether each segment should be on or off. This layer is reshaped to resemble the input data or noise, and then concatenated - which explains why figure \ref{fig:second cgan output 1} is brighter than figure \ref{fig:first cgan output 1}.

\lstinputlisting{22-02-10/Second Model/BasicGeneratorModel.txt}

\lstinputlisting{22-02-10/Second Model/BasicDiscriminatorModel.txt}

\begin{figure}[H]
  \centering

  \begin{subfigure}[b]{0.3\textwidth}
    \includegraphics[width=\textwidth]{22-02-10/Second Model/image_at_epoch_0001.png}
    \caption{Epoch 1}
    \label{fig:second cgan output 1}
  \end{subfigure}%
  \begin{subfigure}[b]{0.3\textwidth}
    \includegraphics[width=\textwidth]{22-02-10/Second Model/image_at_epoch_0015.png}
    \caption{Epoch 15}
    \label{fig:second cgan output 15}
  \end{subfigure}

  \begin{subfigure}[b]{0.3\textwidth}
    \includegraphics[width=\textwidth]{22-02-10/Second Model/image_at_epoch_0050.png}
    \caption{Epoch 50}
    \label{fig:second cgan output 50}
  \end{subfigure}%
  \begin{subfigure}[b]{0.3\textwidth}
    \includegraphics[width=\textwidth]{22-02-10/Second Model/image_at_epoch_0100.png}
    \caption{Epoch 100}
    \label{fig:second cgan output 100}
  \end{subfigure}

  \caption{Example output for labels 1-9, after different training epochs for the second model}
  \label{fig:second cgan output}

\end{figure}

After 15 epochs, in figure \ref{fig:second cgan output 15}, the most distinct digits, 1 and 7, already begin to resemble themselves. Then again in figure \ref{fig:second cgan output 50} more of the digits are recognisable (5 and 4, arguably 2). Finally, after 100 epochs, in figure \ref{fig:second cgan output 100} only 3, 6, and 8 are not recognisable. This is a really clear demonstration of the way that \glspl{gan}, and specifically \glspl{cgan}, train. The discriminator finds it much easier to distinguish certain digits, and therefore the generator learns how to create those digits much faster.

This model took even longer to train, at about four and a half hours. It is important that my next step is getting this model working on Iridis. TensorFlow has a system for saving checkpoints, which will be useful here. I have however managed to demonstrate that my \gls{cgan} training skeleton is functional, and could potentially switch over the more complicated audio dataset.
