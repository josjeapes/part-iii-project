
\section{Background}

\subsection{Generative Models}

Standard machine learning models are discriminative, \cref{equation:discriminative-model}, in that they try to predict the properties $\mathbf{y}$ of data $\mathbf{x}$ from some dataset $\mathcal{X}$. Generative models, \cref{equation:generative-model} are more complicated because they have to capture all the patterns in $\mathbf{x}$ and the way they change with the properties $\mathbf{y}$ in order to actually generate new pieces of data $\mathbf{x}$ with a high probability of belonging to $\mathcal{X}$ and having the properties $\mathbf{y}$.

\begin{equation} \label{equation:discriminative-model}
  P(\mathbf{y}|\mathbf{x})
\end{equation}

\begin{equation} \label{equation:generative-model}
  P(\mathbf{x},\mathbf{y})
\end{equation}

Generative models are more difficult because they have to fully understand the dataset. As an example, if $\mathcal{X}$ is a set of images of human faces $\mathbf{x}$ and $\mathbf{y}$ is eye color than the discriminative model only needs to find the eyes and pick out the color, all the other information can be ignored. By contrast, generating images of human faces that could belong to $\mathcal{X}$ with a given eye color $\mathbf{y}$ also requires knowledge of how every other part of a face looks, and the ability to stitch them all together in a way that looks convincing.

\subsection{Model Optimization}

Most models have tunable parameters, or weights, $\mathbf{w}$ and define some loss function $L(\mathbf{w})$ such that the better the model fits $\mathcal{D}$, the lower $L(\mathbf{w})$ is. For linear regression with no y-intercept on a dataset with one value in $x$ and $y$ for each piece of data, the only weight would be the gradient $m$ of the line of best fit $y = mx$ (though perhaps written as $y = w_0x$). The most common loss function for linear regression is mean squared error \cref{equation:mean-squared-error-loss}. For such a model the optimal weights $\mathbf{w}$ can be found by differentiating by $w_0$, \cref{equation:mean-squared-error-loss-derivative} (which is the reason for the $\frac{1}{2}$ in the loss function, with the $\frac{1}{N}$ being there to normalize the loss for different dataset sizes) and finding the minimum \cref{equation:mean-squared-error-loss-derivative-solved}.

\begin{equation} \label{equation:mean-squared-error-loss}
  L(\mathbf{w}) = \frac{1}{2N} \sum_{i = 1}^{N} (w_0x_i - y_i)^2 \text{, where } N \text{ is the number of pieces of data}
\end{equation}

\begin{equation} \label{equation:mean-squared-error-loss-derivative}
  \frac{\partial L(\mathbf{w})}{\partial w_0} = \frac{1}{N} \sum_{i = 1}^{N} w_0x_i^2 - x_iy_i
\end{equation}

\begin{equation} \label{equation:mean-squared-error-loss-derivative-solved}
  w_0 = \sum_{i = 1}^{N} \frac{y_i}{x_i} = \frac{\bar{\mathbf{y}}}{\bar{\mathbf{x}}}
\end{equation}

Such simple models will generally not be able to fit the data - you can't accurately predict whether an image is of a dog or cat by drawing a straight line through the multidimensional image data. More complicated models typically can't be optimized analytically. Instead, we train them via gradient descent - changing the weights in the direction that leads to the greatest reduction in the loss function in the hope of reaching the minimum. When a model is made of multiple layers, this is often done via backpropagation, which uses the chain rule to carry the optimal weight changes back through the model.

\subsection{Introduction to \glspl{gan}}
\glsreset{gan}

An increasingly common generative model is the \gls{gan} \cite{goodfellow2017nips}. A basic \gls{gan} is an unsupervised learning model, that aims to generate data $\hat{\mathbf{x}}$ that has a high probability $P(\hat{\bm{x}} \in \Chi)$ of belonging to the population $\Chi$ to which the training data $\{\bm{x}_1, ..., \bm{x}_N\} = \mathcal{X} \sim \Chi $ belongs.

\begin{equation} \label{equation:basic-generator}
  G(\mathbf{r};\mathbf{w}_g) = \hat{\mathbf{x}}
\end{equation}

 \begin{equation} \label{equation:basic-discriminator}
  D(\mathbf{z};\mathbf{w}_d) = P(\mathbf{z} \in \mathcal{X}) \text{ where } \mathbf{z} =  \mathbf{x} \sim \mathcal{X} \text{ or } \mathbf{z} = G(\mathbf{r};\mathbf{w}_g)
\end{equation}

 A GAN is actually made of two separate models, a discriminator $D$ (\cref{equation:basic-discriminator}) and a generator $G$ (\cref{equation:basic-generator}). The generator takes a random input $\mathbf{r}$ and outputs a piece of data $\hat{\mathbf{x}}$. The discriminator takes either data from the dataset, $\mathbf{x}$, or data from the generator, $\hat{\mathbf{x}}$, and outputs a probability that the data came from the dataset. The closer the output to one, the higher the confidence that the input belongs to $\mathcal{X}$. A block diagram of this system can be seen in \cref{figure:basic-gan-block-diagram}.

\begin{figure}[H]
  \centering
  \begin{tikzpicture}

    \node[draw,
    circle,
    minimum size = 0.6cm,
    ] (or) at (0,0){$\mathbf{or}$};

    \node[draw,
    minimum width = 2cm,
    minimum height = 1.2cm,
    below = 1cm of or
    ] (generator) {$G(\mathbf{r};\mathbf{w}_g)$};

    \node[draw,
    minimum width = 2cm,
    minimum height = 1.2cm,
    right=1cm of or,
    ] (discriminator) {$D(\mathbf{z};\mathbf{w}_d)$};

    \draw[stealth-] (generator.west) -- ++(-3,0)
      node[midway, above]{random noise $\mathbf{r}$};

    \draw[stealth-] (or.west) -- ++(-3.5,0)
      node[midway, above]{training data $\mathbf{x}\sim\mathcal{X}$};

    \draw[-stealth] (generator.north) -- (or.south)
      node[midway,xshift=-1.7cm]{generated data $\mathbf{\hat x}$};

    \draw[stealth-] (discriminator.west) -- (or.east)
      node[midway,above]{$\mathbf{z}$};

    \draw[-stealth] (discriminator.east) -- ++(3,0)
      node[midway, above]{$P(\mathbf{z}\in\mathcal{X})$};

  \end{tikzpicture}

  \caption{Basic GAN block diagram.}
  \label{figure:basic-gan-block-diagram}
\end{figure}

The discriminator is fed both generated and training set data, and then trained to differentiate them using a loss function, typically binary cross entropy loss (as it is distinguishing between two classes - training data and not training data) as in \cref{equation:basic-discriminator-loss}. The generator is trained to trick the discriminator into thinking the generated data is from the training set using the loss function in \cref{equation:basic-generator-loss} \cite{AGGARWAL2021100004}.

\begin{equation} \label{equation:basic-generator-loss}
  L_G(\mathbf{w}_g) = -\ln{D(G(\mathbf{r};\mathbf{w}_g);\mathbf{w}_d)}
 \end{equation}

\begin{equation} \label{equation:basic-discriminator-loss}
 L_D(\mathbf{w}_d) = -
\begin{cases}
  \ln{D(\mathbf{z};\mathbf{w}_d)} \text{ for } \mathbf{z} \in \mathcal{X}\\
  \ln(1 - D(\mathbf{z};\mathbf{w}_d)) \text{ for } \mathbf{z} \notin \mathcal{X}
\end{cases}
\end{equation}

Notice that for the discriminator's loss to be low $D(G(\mathbf{r};\mathbf{w}_g);\mathbf{w}_d)$ must be low, but for the generator's loss to be low $D(G(\mathbf{r};\mathbf{w}_g);\mathbf{w}_d)$ must be high.
 The strength and the name of \glspl{gan} come from this competition over the value that the discriminator returns for what the generator produces. Although \glspl{gan} are ultimately a generative model and so faced with the more difficult general problem of working out \cref{equation:generative-model} rather than \cref{equation:discriminative-model}, the discriminator reduces part of the problem to a discriminative one - which is much easier to solve - and this is used to train the rest of the model. As a result, \glspl{gan} can be very effective.

 This competition does have the bonus effect that a \gls{gan} doesn't converge on generating the average of the dataset, but rather generates data that could have belonged to the dataset. It is a subtle difference but reduces the blurriness of generated data \cite{DBLP:journals/corr/LedigTHCATTWS16}.

 \subsection{Possible Issues with using a \gls{gan}} \label{section:possible-issues}

A negative side effect of this competition between the two different models is that, compared to other \gls{ml} systems, the behaviour of a \gls{gan} is game-like. Other systems use optimization algorithms to converge on a local minimum loss but there is potential for this to never happen with a \gls{gan} because as one model gains ground the other loses it. In practice, they often end up oscillating back and forth \cite{DBLP:journals/corr/KodaliAHK17} but this is an ongoing area of research and there is no guarantee that the system will converge on something that resembles an optimal solution - it is perfectly possible for a \gls{gan} to be non-convergent.

Mode collapse is the most  common form of non-convergent behaviour in \glspl{gan}. If the dataset is made up of separate clusters, or modes, of data with large gaps between them (for instance different breeds of dog in a dataset of images being used to train a \gls{gan} to produce images of dogs) then the generator might find it much easier to learn how to generate data that matches just one of the modes (or one of the breeds of dog) \cite{9312049}. Eventually, the classifier would learn to reject all pieces of data from that mode (that breed of dog), passing everything else. At this point the generator would simply learn to produce one of the other modes, forgetting the first. No matter how long the system is trained for, it will keep going round in circles and never reach the point where the generator creates data that could belong to any of the modes (images of a random dog breed).

\subsection{\glspl{cgan}} \label{section:cgans}
\glsreset{cgan}

So far, I've discussed basic \glspl{gan}, which are used for unsupervised learning. Such a system would not be sufficient to solve the problem defined in \cref{section:problem-definition} because it provides no control over the instrument family and pitch of the note that is generated - even if the model avoided mode collapse and was able to capture the whole of the dataset, the type of note produced could only be changed by changing the random input $\mathbf{r}$. There would be no user control of the generated sample.

\begin{equation} \label{equation:conditional-generator}
  G(\mathbf{y,r};\mathbf{w}_g) = \hat x
\end{equation}

 \begin{equation} \label{equation:conditional-discriminator}
  D(\mathbf{z,y};\mathbf{w}_d) = P(\mathbf{(z,y)} \in \mathcal{D}) \text{ where } \mathbf{(z,y)} = \mathbf{(x,y)} \sim \mathcal{D} \text{ or } \mathbf{(z,y)} = (G(\mathbf{y,r};\mathbf{w}_g),\mathbf{y})
\end{equation}

The solution to this is called the \gls{cgan} \cite{9034368}. For some labelled dataset $\{(\mathbf{x}_1,\mathbf{y}_1), ...,(\mathbf{x}_N,\mathbf{y}_N)\} = \mathcal{D} \sim \mathrm{D}$ you feed both the random vector $\mathbf{r}$ and a label $\mathbf{y} \sim \mathcal{D}$ into the generator, and feed both the generated data $\hat{\mathbf{x}}$ or the training data $\mathbf{x}$ and the corresponding label $\mathbf{y}$ into the discriminator as can be seen in \cref{figure:conditional-gan-block-diagram}.

\begin{figure}[h]
  \centering
  \begin{tikzpicture}

    \node[draw,
    circle,
    minimum size = 0.6cm,
    ] (or) at (0,0){$\mathbf{or}$};

    \node[draw,
    minimum width = 2cm,
    minimum height = 1.2cm,
    below = 1cm of or
    ] (generator) {$G(\mathbf{y,r};\mathbf{w}_g)$};

    \node[draw,
    minimum width = 2cm,
    minimum height = 1.2cm,
    right=1cm of or,
    ] (discriminator) {$D(\mathbf{z,y};\mathbf{w}_d)$};

    \draw[stealth-] (generator.west) -- ++(-3,0)
      node[midway, above]{random noise $\mathbf{r}$};

    \draw[stealth-] (or.west) -- ++(-3.5,0)
      node[midway, above]{training data $\mathbf{x}_i \sim \mathcal{D}$};

    \draw[-stealth] (generator.north) -- (or.south)
      node[midway,xshift=-1.7cm]{generated data $\hat{\mathbf{x}}$};

    \draw[stealth-] (discriminator.west) -- (or.east)
      node[midway,above]{$\mathbf{z}$};

    \draw[-stealth] (discriminator.east) -- ++(3,0)
      node[midway, above]{$P(\{\mathbf{z},\mathbf{y}\}\in\mathcal{D})$};

      \draw[stealth-] (generator.east) -- ++(1.4,0)
      node[midway, right]{};

    \draw[stealth-] (discriminator.south) -- ++(0,-3)
    node[midway, right]{training label $\mathbf{y}_i \sim \mathcal{D}$};

  \end{tikzpicture}

  \caption{Conditional GAN block diagram.}
  \label{figure:conditional-gan-block-diagram}
\end{figure}

The generator will not know the difference between the random input $\mathbf{r}$ and the label $\mathbf{y}$. However, because the same label is being provided to the discriminator it can learn, using a loss function like \cref{equation:conditional-discriminator-loss}, to only pass input data $\mathbf{z}$ that matches the input label $\mathbf{y}$, and to otherwise reject data even if it would pass with a different label. In this way, the generator will need to learn how to create data for specific labels via a loss function like \cref{equation:conditional-generator-loss}.

\begin{equation} \label{equation:conditional-generator-loss}
  L_G(\mathbf{w}_g) = -\ln{D(y,G(\mathbf{y,r};\mathbf{w}_g);\mathbf{w}_d)}
 \end{equation}

\begin{equation} \label{equation:conditional-discriminator-loss}
 L_D(\mathbf{w}_d) = -
\begin{cases}
  \ln{D(\mathbf{z,y};\mathbf{w}_d)} \text{ for } (\mathbf{z,y}) \in \mathcal{D}\\
  \ln(1 - D(\mathbf{z},\mathbf{y};\mathbf{w}_d)) \text{ for } (\mathbf{z,y}) \notin \mathcal{D}
\end{cases}
\end{equation}


Using a \gls{cgan} also reduces the problems that mode collapse could cause. The major modes that my system could collapse into are the instrument families and pitches - the two pieces of information that need to be used as labels to solve the problem, forcing the generator and discriminator to understand the full range of the dataset rather than just one pitch or instrument family. The system could still slip into mode collapse within instrument families - collapsing to a single instrument (see \cref{table:nsynth:features} for the difference between instrument families and instruments) but solving this is beyond the definition of the problem my project is trying to solve.

Related to the above is the fact that the generator is a continuous function - as you change the labels $\mathbf{y}$ it gradually morphs from one to the next \cite{radford2016unsupervised}. The result of this is that as long as the system can accurately generate notes with adjacent pitches from the standard pitch range that is included in the NSynth dataset, it will be able to create samples for any pitch in between. Similarly, it should actually be possible to create notes that have the timbre of two different instrument families - say half guitar, half keyboard.

\subsection{Generated Sample Assessment}

In a paper introducing the GANSynth, \cite{DBLP:journals/corr/abs-1902-08710}, five standard metric for evaluating similar generative models were listed:

\begin{itemize}
  \item \textbf{Human Evaluation} - Audio quality is difficult to measure objectively  and so human checks on audio quality are important.
  \item \textbf{\gls{ndb}} - A measure of the diversity of the generated samples \cite{DBLP:journals/corr/abs-1805-12462}.
  \item \textbf{\gls{is}} - A de-facto standard for measuring \glspl{gan} \cite{NIPS2016_8a3363ab}, that uses a pretrained classifier.
  \item \textbf{\gls{pa} and \gls{pe}} - pitch accuracy and pitch entropy are both seperately measured in the GANSynth paper.
  \item \textbf{\gls{fid}} - Another proposed metric \cite{NIPS2017_8a1d6947} "based on the 2-Wasserstein (or Frechet) distance between multivariate Gaussians fit to features extracted from a pretrained Inception classifier and show that this metric correlates with perceptual quality and diversity on synthetic distribution". The GANSynth paper uses pitch-classifier features for this and the \gls{is} instead of Inception features.
\end{itemize}

Sitcking to these metrics will help me to compare my own model to the existing ones.

\subsection{Preprocessing}\label{section:preprocessing}

There are a number of different ways that audio data can be represented. In the NSynth dataset it is in the time domain ${x_0,...,x_N}$ so will require preprocessing for other forms. Examples can be seen in \cref{figure:example-audio-time-series}.

\begin{figure}[H]
  \noindent
  \makebox[\textwidth]{\import{../Experimental Research/Classifier/figures}{example-audio.pgf}}
  \caption{Example time series audio data.}
  \label{figure:example-audio-time-series}
\end{figure}

Other possible representations include the frequency domain ${X_0,...,X_N}$, which is achieved by applying the \gls{dft}, \cref{equation:dft}.

\begin{equation} \label{equation:dft}
  \begin{split}
  X_k = \sum_{n=0}^{N} x_n \cdot e^{- 2 i \pi \frac{n}{N} k} \text{ for } k = 0 \text{ to } N
\end{split}
 \end{equation}

It would also be possible to use the envelope of either of the above data forms. While it will be worth testing both the above forms of audio data in my project, the most successful recent models \cite{DBLP:journals/corr/abs-1902-08710} all transform the data into a spectrogram (or some similar form of short time frequency data), a representation of instantaneous frequency over time, examples of which can be seen in \cref{figure:example-audio-spectrogram}. This is done by taking the square of the magnitude of the \gls{stft} (the \gls{dft} of a signal for a small window of time).

\begin{figure}[H]
  \noindent
  \makebox[\textwidth]{\import{../Experimental Research/Classifier/figures}{example-spectrogram.pgf}}
  \caption{The example audio from \cref{figure:example-audio-time-series} data as spectrograms.}
  \label{figure:example-audio-spectrogram}
\end{figure}

The reason for using spectrograms make sense intuitively - the system might need to make decisions using both time and frequency data (frequency information is going to be useful in pitch detection, but different instrument families will have different envelope shapes in the time domain) and with the other data forms the system would have to perform the $\text{time} \leftrightarrow \text{frequency}$ transformations itself. Similar projects have found that spectrogram \glspl{gan} are more effective than WaveNet on a number of common metrics \cite{DBLP:journals/corr/abs-1902-08710}.

The generator will need to either generate audio data which is then preprocessed in the exact same way as the training data, or in the same form as the preprocessed audio data. These methods should both be attempted and compared. In the latter case, it is necessary to transform the data back into the time domain. Phase information is lost when squaring the magnitude of the \gls{stft}, but there are multiple different methods for getting the time series data back from a spectrogram \cite{4217495}.
