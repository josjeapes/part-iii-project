\appendix

\counterwithin{table}{section}

\section{NSynth Dataset} \label{section:nsynth-dataset}

The Nsynth dataset is available as .tfrecord files which means the data is more coherent on disk than if seperate files were used for each note, increasing the read speed. .tfrecords are specfically optimized for use with tensorflow, and allow streaming batches from disk to memory before moving on to the next batch.

The Nsynth datset's instrument families are listed in \cref{table:nsynth:instrument-families}.

\begin{table}[H]

  \centering

  \begin{tabularx}{\textwidth}{ | l | l | X | }

    \hline

    Feature & Type & Description \\

    \hlineB{3}

    Note & Integer & A unqiue ID \\
    \hline
    Audio & Float array & The audio data itself, normalized to the range -1 to 1 \\
    \hline
    Instrument family & Integer & ID of the instrument family the note was played/synthesized on, listed in \cref{table:nsynth:instrument-families}. \\
    \hline
    Pitch & Integer & The \gls{midi} pitch, in the range 0 - 127, translated to frequency by \cref{equation:midi-pitch-to-frequency} \\
    \hline
    Instrument & Integer & A unqiue ID for the instrument that note was played on. \\
    \hline
    Qualities & Integer array & A list of the qualities present in the note, listed in \cref{table:nsynth:note-qualities}. \\
    \hline
    Velocity & Integer & The \gls{midi} velocity, in the range 0 - 127. \\
    \hline
    Sample rate & Integer & the samples per second of the audio data. \\
    \hline
    Instrument source & Integer & ID of the source of the note, listed in \cref{table:nsynth:instrument-sources}. \\

    \hline

  \end{tabularx}

  \caption{NSynth data features. (sourced from \fullcite{nsynth2017})}

  \label{table:nsynth:features}

\end{table}

\begin{table}[H]

\centering

\begin{tabular}{ | r | r | }

  \hline

  Index & Instrument family \\

  \hline

  0 & Bass \\
  1 & Brass \\
  2 & Flute \\
  3 & Guitar \\
  4 & Keyboard \\
  5 & Mallet \\
  6 & Organ \\
  7 & Reed \\
  8 & String \\
  9 & Synth lead \\
  10 & Vocal \\

  \hline

\end{tabular}

\caption{NSynth instrument families. (sourced from \fullcite{nsynth2017})}

\label{table:nsynth:instrument-families}

\end{table}

\begin{table}[H]

\centering

\begin{tabular}{ | r | r | }

  \hline

  Index & Instrument source \\

  \hline

  0 & Acoustic \\
  1 & Electronic \\
  2 & Synthetic \\

  \hline

\end{tabular}

\caption{NSynth instrument sources. (sourced from \fullcite{nsynth2017})}

\label{table:nsynth:instrument-sources}

\end{table}

\begin{table}[H]

  \centering

\begin{tabularx}{\textwidth}{ | r | r | X | }

  \hline

  Index & Note quality & Description \\

  \hlineB{3}

  0 & Bright & A large amount of high frequency content and strong upper harmonics. \\
  \hline
  1 & Dark & A distinct lack of high frequency content, giving a muted and bassy sound. Also sometimes described as 'Warm'. \\
  \hline
  2 & Distortion & Waveshaping that produces a distinctive crunchy sound and presence of many harmonics. Sometimes paired with non-harmonic noise.\\
  \hline
  3 & Fast decay & Amplitude envelope of all harmonics decays substantially before the 'note-off' point at 3 seconds. \\
  \hline
  4 & Long release & Amplitude envelope decays slowly after the 'note-off' point, sometimes still present at the end of the sample 4 seconds. \\
  \hline
  5 & Multiphonic & Presence of overtone frequencies related to more than one fundamental frequency. \\
  \hline
  6 & Nonlinear env & Modulation of the sound with a distinct envelope behavior different than the monotonic decrease of the note. Can also include filter envelopes as well as dynamic envelopes. \\
  \hline
  7 & Percussive & A loud non-harmonic sound at note onset. \\
  \hline
  8 & Reverb & Room acoustics that were not able to be removed from the original sample. \\
  \hline
  9 & Tempo-synced & Rhythmic modulation of the sound to a fixed tempo. \\

  \hline

\end{tabularx}

\caption{NSynth note qualities, descriptions taken from the NSynth documentation. (sourced from \fullcite{nsynth2017})}

\label{table:nsynth:note-qualities}

\end{table}

\begin{table}[H]

  \centering

  \begin{tabular}{ | r | r r r | r | }

    \hline

    Family & Acoustic & Electronic & Synthetic & Total \\

    \hline

    Bass & 200 & 8,387 & 60,368 & 68,955 \\
    Brass & 13,760 & 70 & 0 & 13,830 \\
    Flute & 6,572 & 35 & 2,816 & 9,423 \\
    Guitar & 13,343 & 16,805 & 5,275 & 35,423 \\
    Keyboard & 8,508 & 42,645 & 3,838 & 54,991 \\
    Mallet & 27,722 & 5,581 & 1,763 & 35,066 \\
    Organ & 176 & 36,401 & 0 & 36,577 \\
    Reed & 14,262 & 76 & 528 & 14,866 \\
    String & 20,510 & 84 & 0 & 20,594 \\
    Synth Lead & 0 & 0 & 5,501 & 5,501 \\
    Vocal & 3,925 & 140 & 6,688 & 10,753 \\

    \hline

    Total & 108,978 & 110,224 & 86,777 & 305,979 \\

    \hline

  \end{tabular}

  \caption{Sample count for each Nsynth instrument family, by source. (sourced from \fullcite{nsynth2017})}

  \label{table:nsynth:instrument-sample-count}

  \end{table}

  \newpage

\section{Logs}

\subsection{Instrument Family classification Model}

\lstinputlisting[caption={Instrument family classification model log.},label={listing:instrument-classification}]{../Experimental Research/Classifier/log.txt}

\subsection{Instrument Family Regression Model}

\lstinputlisting[caption={Pitch Regression Model Log},label={listing:pitch-regression},linerange={1-40,1000-1034}]{../Experimental Research/Regression Model/log.txt}

\subsection{Instrument Family Classification Model}

\lstinputlisting[caption={Pitch Classification Model Log},label={listing:pitch-classification},linerange={1-40,1000-1031}]{../Experimental Research/Pitch Classifier/log.txt}