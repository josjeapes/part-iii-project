\section{Progress}


\subsection{Instrument Family Classifier} \label{section:instrument-model}

The discriminator is going to need to be able to differentiate between instrument family classes, so I decided to familiarize myself with the dataset and with TensorFlow \cite{tensorflow2015whitepaper} (the \gls{ml} library I'm using) by building a classification model to detect the instrument family of a sample. The NSynth dataset is divided into a training dataset with 289,205 notes, a validation dataset with 12,678 notes, and a test dataset with 4,096 notes. Both this model and the ones in \cref{section:pitch-models} were actually trained on the validation set and validated and tested on the test set. This is because the smaller dataset is sufficient for model testing and, while as discussed in \cref{section:nsynth-dataset} the dataset file format can be streamed batch by batch, shuffling the dataset fully requires loading it all into memory which takes time. For the final model I will shuffle the data once and use the shuffled dataset.

\begin{figure}[H]
  \begin{center}
    \import{../Experimental Research/Classifier/figures}{epoch-loss.pgf}
  \end{center}
  \caption{Loss of the training and validation data against epoch for instrument family classification model.}
  \label{figure:instrument-classifier-loss}
\end{figure}

\begin{figure}[H]
  \begin{center}
    \import{../Experimental Research/Classifier/figures}{example-predictions.pgf}
  \end{center}
  \caption{Example model prediction for a random piece of test data.}
  \label{figure:instrument-classifier-prediction}
\end{figure}

The model can be seen in \cref{listing:instrument-classification}, alongside the loss for each epoch, also pictured in \cref{figure:instrument-classifier-loss}. The model is the standard, open-source TensorFlow classification model \cite{tensorflow2015whitepaper} with image processing layers at the start (the audio data is being preprocessed into). The resizing layer resizes the image down to 32 by 32 (in the final model, more image data than this will probably be needed), and the conv2D layers are to allow the system to learn to apply a filter (convoluting a kernel over the input) to the image - often this ends up being used for edge detection or similar. The max-pooling layer is there to reduce variance, and the flatten layer reduces the '2D' image network down to a 1D one. The dropout layers randomly don't feed values forward when training, to reduce overfitting. The final dense layer has eleven outputs for the eleven instrument families. The softmax function is applied to the output afterwards, to get probabilities out of the system.

The overall accuracy of the model after 10 epochs is 97\%, as can be seen in the log. Trained on the larger training set for a bit longer, and the accuracy would increase even further. That this model works as well as it does means that, hopefully, the discriminator will not struggle with the instrument family labels.
\begin{figure}[H]
  \begin{center}
    \import{../Experimental Research/Classifier/figures}{heatmap.pgf}
  \end{center}
  \caption{Actual classes against model prediction.}
  \label{figure:instrument-classifier-heatmap}
\end{figure}

As can be seen in \cref{figure:instrument-classifier-loss}, there is no overfitting and the validation loss does decrease with the training loss.  \cref{figure:instrument-classifier-heatmap} is a slightly more detailed view of the model accuracy than just the percentage from the log and also shows how the test set has no "Synth lead" instruments, the smallest instrument family according to \cref{table:nsynth:instrument-sample-count}. An example of the model output, with softmax applied, can be seen in \cref{figure:instrument-classifier-prediction}.



\subsection{Pitch Detection} \label{section:pitch-models}

Pitch is a continuous value and so the input to the generator needs to be a continuous value in order to be able to produce microtones. This does not, however, mean that the discriminator input needs to be continuous. The dataset, as detailed in \cref{table:nsynth:features}, only contains standard pitches which means that if the generator were to create microtones during training the discriminator would learn to reject all of them based off the label. As discussed at the end of \cref{section:cgans}, this will not prevent generating microtones after the model is trained because it will interpolate timbre between adjacent pitches. Because the dataset is limited to 128 standard pitches and the generator will only need to being synthesizing these pitches during training, it would also be possible for the discriminator to take discrete pitch class as an input (rather than one continuous pitch variable). This means comparing the effectiveness of regression and classification at detecting pitch is an important step in choosing a \gls{gan} model.

\subsubsection{Regression}

First, I built and tested the regression model, which can be seen in \cref{listing:pitch-regression}. It is based on TensorFlow's open-source regression model \cite{tensorflow2015whitepaper}, with the image processing input from the previous section. The model outputs a single value - its prediction of the input's pitch in Hz. I have taken this value and rounded it to the nearest standard pitch value, although it can be seen in the log that this doesn't really affect the average pitch error of 109Hz after 500 epochs. After rounding in this way, only 6\% of the pitches are correct. This implies that the discriminator would struggle with a continuous label for pitch, resulting in the generator not producing pitch accurate samples.

\begin{figure}[H]
  \centering
    \import{../Experimental Research/Regression Model/figures}{epoch-loss.pgf}
  \caption{Loss of the training and validation data against epoch, for pitch regression model.}
  \label{figure:pitch-regresser-loss}
\end{figure}

\cref{figure:pitch-regresser-predictions} shows random examples predictions made be the model. The bottom right one is an example of the model getting the more or less the correct pitch, but the others are considerably off. It is possible this model would do better with less blurred image input, but that risks overfitting.

\begin{figure}[H]
  \noindent
  \makebox[\textwidth]{\import{../Experimental Research/Regression Model/figures}{predictions.pgf}}
  \caption{Actual pitch, predicted (continuous) pitch and the closest standard pitch of some example test data.}
  \label{figure:pitch-regresser-predictions}
\end{figure}

\subsubsection{Classification}

The classification model, seen in \cref{listing:pitch-classification}, is exactly the same as for \cref{section:instrument-model} with the exception that it has 128 class outputs, not eleven.

\begin{figure}[H]
  \centering
    \import{../Experimental Research/Pitch Classifier/figures}{epoch-loss.pgf}
    \caption{Loss of the training and validation data against epoch, for pitch classification model.}
  \label{figure:pitch-classifier-loss}
\end{figure}

As seen in \cref{figure:pitch-classifier-loss}, the classifier model does exhibit problems with overfitting. This would be fixed by the standard practice of stopping at the point at which the model is no longer improving (I disabled this behaviour for the sake of this test, so it gets the same amount of data as the regression model). Overfitting should also be much less of a problem with the much larger actual training set, and when being trained against generated content. Despite overfitting, it is still significantly more accurate than the regression model - after 500 epochs, it has an accuracy of 82\% and an average pitch error of 27.40Hz. The frequency of middle C, $C_4=261.63 Hz$, and the next semitone up is  $C_4^\#=277.18 Hz$ - a difference smaller than the average pitch error of the model. Training for longer on the actual dataset, with 20x the samples, should get better results.

Some random example predictions can be seen in \cref{figure:pitch-classifier-predictions}.

\begin{figure}[H]
  \noindent
  \makebox[\textwidth]{\import{../Experimental Research/Pitch Classifier/figures}{predictions.pgf}}
  \caption{The spectrogram for four seperate random pieces of data (left), as well as the network output before softmax is applied (centre), and the network output after softmax is applied (right) both plotted against the actual pitch.}
  \label{figure:pitch-classifier-predictions}
\end{figure}

\subsubsection{Comparision}

The classification model achieves superior results to the regression model when predicting pitch, so it makes sense to treat the pitch inputs to the discriminator as discrete classes rather than one continuous value.