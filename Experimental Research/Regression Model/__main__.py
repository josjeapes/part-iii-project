from contextlib import redirect_stdout
import os
import pathlib

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import tensorflow as tf

from tensorflow.keras import layers
from tensorflow.keras import models
from IPython import display

import matplotlib


matplotlib.use("pgf")
matplotlib.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
})

DATA_PATH = 'D:/Uni/Y3/Part III Project/Experimental Research/Classifier/data/nsynth'

BATCH_SIZE = 64
EPOCHS = 500

AUTOTUNE = tf.data.AUTOTUNE

PITCH_COUNT = 128

def midi_pitch_to_freq(pitch):
  return 440 * 2 ** ((pitch - 69) / 12)

def get_data(path, filename, url):

  data_dir = pathlib.Path(path)
  if not data_dir.exists():
    tf.keras.utils.get_file(
        filename,
        origin=url,
        extract=True,
        cache_dir='.', cache_subdir=path)

def parse_test(data):
  parsed = tf.io.parse_single_example(data, features)
  return parsed['audio'], midi_pitch_to_freq(parsed['pitch'])

def get_spectrogram(waveform):
  spectrogram = tf.signal.stft(waveform, frame_length=255, frame_step=128)
  spectrogram = tf.abs(spectrogram)
  spectrogram = spectrogram[..., tf.newaxis]
  return spectrogram

def preproccess_spectrogram(audio, pitch):
  spectrogram = get_spectrogram(audio)
  return spectrogram, pitch

def plot_spectrogram(spectrogram, ax):
  if len(spectrogram.shape) > 2:
    assert len(spectrogram.shape) == 3
    spectrogram = np.squeeze(spectrogram, axis=-1)
  # Convert the frequencies to log scale and transpose, so that the time is
  # represented on the x-axis (columns).
  # Add an epsilon to avoid taking a log of zero.
  log_spec = np.log(spectrogram.T + np.finfo(float).eps)
  height = log_spec.shape[0]
  width = log_spec.shape[1]
  X = np.linspace(0, np.size(spectrogram), num=width, dtype=int)
  Y = range(height)
  ax.pcolormesh(X, Y, log_spec)

def plot_data_examples():
  draw_data = []
  for data in test_data.take(9):
    draw_data.append(data)

  i = 0
  fig, axes = plt.subplots(3, 3, figsize=(10,10))
  for (audio, pitch) in draw_data:
    axes[i // 3][i % 3].plot(audio)
    axes[i // 3][i % 3].set_title(f'{pitch:.2f}Hz')
    i += 1

  i = 0
  fig, axes = plt.subplots(3, 3, figsize=(10,10))
  for (audio, pitch) in draw_data:
    spectrogram, pitch = preproccess_spectrogram(audio, pitch)
    plot_spectrogram(spectrogram, axes[i // 3][i % 3])
    axes[i // 3][i % 3].set_title(f'{pitch:.2f}Hz')
    axes[i // 3][i % 3].axis('off')
    i += 1

def find_nearest(array, value):
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return array[idx]

features = {
  'pitch': tf.io.FixedLenFeature([], tf.int64),
  'audio': tf.io.FixedLenSequenceFeature([], tf.float32, allow_missing=True)
}

pitches = midi_pitch_to_freq(np.arange(0, PITCH_COUNT))

get_data(DATA_PATH, "train.tfrecord",
  "http://download.magenta.tensorflow.org/datasets/nsynth/nsynth-train.tfrecord")
get_data(DATA_PATH, "valid.tfrecord",
  "http://download.magenta.tensorflow.org/datasets/nsynth/nsynth-valid.tfrecord")
get_data(DATA_PATH, "test.tfrecord",
  "http://download.magenta.tensorflow.org/datasets/nsynth/nsynth-test.tfrecord")

# use test data for now as it is much smaller so shuffle takes less time
train_data = tf.data.TFRecordDataset(DATA_PATH + "/valid.tfrecord")
val_data = tf.data.TFRecordDataset(DATA_PATH + "/test.tfrecord")
test_data = tf.data.TFRecordDataset(DATA_PATH + "/test.tfrecord")

test_data = test_data.shuffle(buffer_size=3000)
train_data = train_data.shuffle(buffer_size=10000)
val_data = val_data.shuffle(buffer_size=3000)

train_data = train_data.map(parse_test, num_parallel_calls=AUTOTUNE)
val_data = val_data.map(parse_test, num_parallel_calls=AUTOTUNE)
test_data = test_data.map(parse_test, num_parallel_calls=AUTOTUNE)

plot_data_examples()

train_data = train_data.map(preproccess_spectrogram, num_parallel_calls=AUTOTUNE)
val_data = val_data.map(preproccess_spectrogram, num_parallel_calls=AUTOTUNE)
test_data = test_data.map(preproccess_spectrogram, num_parallel_calls=AUTOTUNE)

for (spectrogram, family) in train_data.take(1):
  input_shape = spectrogram.shape

train_data = train_data.batch(BATCH_SIZE)
val_data = val_data.batch(BATCH_SIZE)

train_data = train_data.cache().prefetch(AUTOTUNE)
val_data = val_data.cache().prefetch(AUTOTUNE)

model = models.Sequential([
    layers.Input(shape=input_shape),
    layers.Resizing(32, 32),
    layers.Conv2D(32, 3, activation='relu'),
    layers.Conv2D(64, 3, activation='relu'),
    layers.MaxPooling2D(),
    layers.Dropout(0.25),
    layers.Flatten(),
    layers.Dense(128, activation='relu'),
    layers.Dropout(0.5),
    layers.Dense(20),
    layers.Dense(1),
])

model.summary()

model.compile(
    optimizer=tf.keras.optimizers.Adam(),
    loss=tf.keras.losses.MeanSquaredError(),
    metrics=[tf.keras.metrics.RootMeanSquaredError()],
)

history = model.fit(
    train_data,
    validation_data=val_data,
    epochs=EPOCHS,
    callbacks=tf.keras.callbacks.EarlyStopping(verbose=1, patience=1000),
    verbose=2
)

plt.figure()
plt.figure(figsize=(5.5,4.5))
metrics = history.history
plt.plot(history.epoch, metrics['loss'], metrics['val_loss'])
plt.legend(['Training loss', 'Validation loss'])
plt.title('Training and validation loss against epoch')
plt.xlabel('Epoch')
plt.ylabel("Loss (mean squared error)")

plt.savefig('figures/epoch-loss.pgf')

test_spectrogram = []
test_pitch = []

for spectrogram, pitch in test_data:
  test_spectrogram.append(spectrogram.numpy())
  test_pitch.append(pitch.numpy())

test_spectrogram = np.array(test_spectrogram)
test_pitch = np.array(test_pitch)

y_pred = model.predict(test_spectrogram).flatten()
y_true = test_pitch

y_pred_closest = []

for pred in y_pred:
  y_pred_closest.append(find_nearest(pitches, pred))

total_correct = 0

for i in range(len(y_pred)):
  if y_true[i] == y_pred_closest[i]:
    total_correct += 1

total_correct /= len(y_pred)

print(f'Test set accuracy: {total_correct:.0%}')

average_err = sum(np.abs(y_pred - y_true)) / len(y_true)
print(f'Average pitch error: {average_err:.2f}Hz')

average_err_closest = sum(np.abs(y_pred_closest - y_true)) / len(y_true)
print(f'Average pitch error closest: {average_err_closest:.2f}Hz')

test_data = test_data.batch(1)

i = 0
fig, axes = plt.subplots(2, 2, figsize=(8,8), subplot_kw=dict(rasterized=True))
for spectrogram, pitch in test_data.take(4):
  prediction = model(spectrogram)
  plot_spectrogram(spectrogram[0], axes[i // 2][i % 2])

  closest_note = find_nearest(pitches, prediction[0][0])

  axes[i // 2][i % 2].set_title(f'A: {pitch[0]:.2f}Hz, P: {prediction[0][0]:.2f}Hz, C: {closest_note:.2f}Hz')
  axes[i // 2][i % 2].axes.xaxis.set_ticks([])
  axes[i // 2][i % 2].axes.yaxis.set_ticks([])
  axes[i // 2][i % 2].set_xlabel("Time")
  axes[i // 2][i % 2].set_ylabel("Frequency")
  i += 1
fig.suptitle("Actual, predicted, and closest standard pitch for example test data")
plt.savefig('figures/predictions.pgf', dpi=1000)

plt.show()