#!/bin/bash

module load python

python -V

python -c 'import tensorflow as tf; print(tf.__version__)'

python __main__.py > output.txt
