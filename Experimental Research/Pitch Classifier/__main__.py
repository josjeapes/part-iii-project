from contextlib import redirect_stdout
import os
import pathlib

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import tensorflow as tf

from tensorflow.keras import layers
from tensorflow.keras import models
from IPython import display

import matplotlib


matplotlib.use("pgf")
matplotlib.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
})

DATA_PATH = 'D:/Uni/Y3/Part III Project/Experimental Research/Classifier/data/nsynth'

BATCH_SIZE = 64
EPOCHS = 500

AUTOTUNE = tf.data.AUTOTUNE

PITCH_COUNT = 128

def midi_pitch_to_freq(pitch):
  return 440 * 2 ** ((pitch - 69) / 12)

def get_data(path, filename, url):

  data_dir = pathlib.Path(path)
  if not data_dir.exists():
    tf.keras.utils.get_file(
        filename,
        origin=url,
        extract=True,
        cache_dir='.', cache_subdir=path)

def parse_test(data):
  parsed = tf.io.parse_single_example(data, features)
  return parsed['audio'], parsed['pitch']

def get_spectrogram(waveform):
  spectrogram = tf.signal.stft(waveform, frame_length=255, frame_step=128)
  spectrogram = tf.abs(spectrogram)
  spectrogram = spectrogram[..., tf.newaxis]
  return spectrogram

def preproccess_spectrogram(audio, pitch):
  spectrogram = get_spectrogram(audio)
  return spectrogram, pitch

def plot_spectrogram(spectrogram, ax):
  if len(spectrogram.shape) > 2:
    assert len(spectrogram.shape) == 3
    spectrogram = np.squeeze(spectrogram, axis=-1)
  # Convert the frequencies to log scale and transpose, so that the time is
  # represented on the x-axis (columns).
  # Add an epsilon to avoid taking a log of zero.
  log_spec = np.log(spectrogram.T + np.finfo(float).eps)
  height = log_spec.shape[0]
  width = log_spec.shape[1]
  X = np.linspace(0, np.size(spectrogram), num=width, dtype=int)
  Y = range(height)
  ax.pcolormesh(X, Y, log_spec)

def plot_data_examples():
  draw_data = []
  for data in test_data.take(9):
    draw_data.append(data)

  i = 0
  fig, axes = plt.subplots(3, 3, figsize=(10,10))
  for (audio, pitch) in draw_data:
    axes[i // 3][i % 3].plot(audio)
    axes[i // 3][i % 3].set_title(f'{pitches[pitch]:.2f}Hz')
    i += 1

  i = 0
  fig, axes = plt.subplots(3, 3, figsize=(10,10))
  for (audio, pitch) in draw_data:
    spectrogram, pitch = preproccess_spectrogram(audio, pitch)
    plot_spectrogram(spectrogram, axes[i // 3][i % 3])
    axes[i // 3][i % 3].set_title(f'{pitches[pitch]:.2f}Hz')
    axes[i // 3][i % 3].axis('off')
    i += 1

features = {
  'pitch': tf.io.FixedLenFeature([], tf.int64),
  'audio': tf.io.FixedLenSequenceFeature([], tf.float32, allow_missing=True)
}

pitches = midi_pitch_to_freq(np.arange(0, PITCH_COUNT))

get_data(DATA_PATH, "train.tfrecord",
  "http://download.magenta.tensorflow.org/datasets/nsynth/nsynth-train.tfrecord")
get_data(DATA_PATH, "valid.tfrecord",
  "http://download.magenta.tensorflow.org/datasets/nsynth/nsynth-valid.tfrecord")
get_data(DATA_PATH, "test.tfrecord",
  "http://download.magenta.tensorflow.org/datasets/nsynth/nsynth-test.tfrecord")

# use test data for now as it is much smaller so shuffle takes less time
train_data = tf.data.TFRecordDataset(DATA_PATH + "/valid.tfrecord")
val_data = tf.data.TFRecordDataset(DATA_PATH + "/test.tfrecord")
test_data = tf.data.TFRecordDataset(DATA_PATH + "/test.tfrecord")

test_data = test_data.shuffle(buffer_size=3000)
train_data = train_data.shuffle(buffer_size=10000)
val_data = val_data.shuffle(buffer_size=3000)

train_data = train_data.map(parse_test, num_parallel_calls=AUTOTUNE)
val_data = val_data.map(parse_test, num_parallel_calls=AUTOTUNE)
test_data = test_data.map(parse_test, num_parallel_calls=AUTOTUNE)

plot_data_examples()

train_data = train_data.map(preproccess_spectrogram, num_parallel_calls=AUTOTUNE)
val_data = val_data.map(preproccess_spectrogram, num_parallel_calls=AUTOTUNE)
test_data = test_data.map(preproccess_spectrogram, num_parallel_calls=AUTOTUNE)

for (spectrogram, family) in train_data.take(1):
  input_shape = spectrogram.shape
num_labels = len(pitches)

train_data = train_data.batch(BATCH_SIZE)
val_data = val_data.batch(BATCH_SIZE)

train_data = train_data.cache().prefetch(AUTOTUNE)
val_data = val_data.cache().prefetch(AUTOTUNE)

model = models.Sequential([
    layers.Input(shape=input_shape),
    layers.Resizing(32, 32),
    layers.Conv2D(32, 3, activation='relu'),
    layers.Conv2D(64, 3, activation='relu'),
    layers.MaxPooling2D(),
    layers.Dropout(0.25),
    layers.Flatten(),
    layers.Dense(128, activation='relu'),
    layers.Dropout(0.5),
    layers.Dense(num_labels),
])

model.summary()

model.compile(
    optimizer=tf.keras.optimizers.Adam(),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=['accuracy']
)

history = model.fit(
    train_data,
    validation_data=val_data,
    epochs=EPOCHS,
    callbacks=tf.keras.callbacks.EarlyStopping(verbose=1, patience=1000),
    verbose=2
)

plt.figure(figsize=(5.5,4.5))
metrics = history.history
plt.plot(history.epoch, metrics['loss'], metrics['val_loss'])
plt.legend(['Training loss', 'Validation loss'])
plt.title('Training and validation loss against epoch')
plt.xlabel('Epoch')
plt.ylabel("Loss (categorical crossentropy)")

plt.savefig('figures/epoch-loss.pgf')

test_spectrogram = []
test_pitch = []

for spectrogram, pitch in test_data:
  test_spectrogram.append(spectrogram.numpy())
  test_pitch.append(pitch.numpy())

test_spectrogram = np.array(test_spectrogram)
test_pitch = np.array(test_pitch)

y_pred = np.argmax(model.predict(test_spectrogram), axis=1)
y_true = test_pitch

test_acc = sum(y_pred == y_true) / len(y_true)
print(f'Test set accuracy: {test_acc:.0%}')

average_err = sum(np.abs(pitches[y_pred] - pitches[y_true])) / len(y_true)
print(f'Average pitch error: {average_err:.2f}Hz')

test_data = test_data.batch(1)

i = 0
fig, axes = plt.subplots(4, 3, figsize=(7,8), subplot_kw=dict(rasterized=True))
fig.tight_layout(pad=4, h_pad = 3.5, w_pad = 3)
fig.suptitle("Classification model pitch predictions for random test data")
for spectrogram, pitch in test_data.take(4):
  prediction = model(spectrogram)
  plot_spectrogram(spectrogram[0], axes[i][0])
  axes[i][0].axes.xaxis.set_ticks([])
  axes[i][0].axes.yaxis.set_ticks([])
  axes[i][0].set_xlabel("Time")
  axes[i][0].set_ylabel("Frequency")

  axes[i][1].axvline(x=pitches[pitch], label='Actual pitch', c='r')
  axes[i][1].plot(pitches, prediction[0], label='Network output')
  axes[i][1].set_title(f'{pitches[pitch]:.2f}Hz')
  axes[i][1].set_xlabel("Pitch (Hz)")
  axes[i][1].set_ylabel("Network output")

  axes[i][2].axvline(x=pitches[pitch], label='Actual pitch', c='r')
  axes[i][2].plot(pitches, tf.nn.softmax(prediction[0]), label='softmax output')
  axes[i][2].set_xlabel("Pitch (Hz)")
  axes[i][2].set_ylabel("Confidence")

  i += 1

axes[0][1].legend()
axes[0][2].legend()
plt.savefig('figures/predictions.pgf', dpi=1000)

plt.show()