from contextlib import redirect_stdout
import os
import pathlib

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import tensorflow as tf
from tensorflow.core.framework.types_pb2 import DT_COMPLEX128

from tensorflow.keras import layers
from tensorflow.keras import models
from IPython import display

import matplotlib

matplotlib.use("pgf")
matplotlib.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
})

DATA_PATH = 'D:/Uni/Y3/Part III Project/Experimental Research/Classifier/data/nsynth'

BATCH_SIZE = 64
EPOCHS = 10

AUTOTUNE = tf.data.AUTOTUNE

families = [
  'bass',
  'brass',
  'flute',
  'guitar',
  'keyboard',
  'mallet',
  'organ',
  'reed',
  'string',
  'synth lead',
  'vocal'
]

features = {
  'instrument_family': tf.io.FixedLenFeature([], tf.int64),
  'audio': tf.io.FixedLenSequenceFeature([], tf.float32, allow_missing=True)
}

def get_data(path, filename, url):

  data_dir = pathlib.Path(path)
  if not data_dir.exists():
    tf.keras.utils.get_file(
        filename,
        origin=url,
        extract=True,
        cache_dir='.', cache_subdir=path)

def parse_test(data):
  parsed = tf.io.parse_single_example(data, features)
  return parsed['audio'], parsed['instrument_family']

def get_spectrogram(waveform):
  spectrogram = tf.signal.stft(waveform, frame_length=255, frame_step=128)
  spectrogram = tf.abs(spectrogram)
  spectrogram = spectrogram[..., tf.newaxis]
  return spectrogram

def preproccess_spectrogram(audio, family):
  spectrogram = get_spectrogram(audio)
  return spectrogram, family

def plot_spectrogram(spectrogram, ax):
  if len(spectrogram.shape) > 2:
    assert len(spectrogram.shape) == 3
    spectrogram = np.squeeze(spectrogram, axis=-1)
  # Convert the frequencies to log scale and transpose, so that the time is
  # represented on the x-axis (columns).
  # Add an epsilon to avoid taking a log of zero.
  log_spec = np.log(spectrogram.T + np.finfo(float).eps)
  height = log_spec.shape[0]
  width = log_spec.shape[1]
  X = np.linspace(0, np.size(spectrogram), num=width, dtype=int)
  Y = range(height)
  ax.pcolormesh(X, Y, log_spec)

def plot_data_examples():
  draw_data = []
  for data in test_data.take(4):
    draw_data.append(data)

  i = 0
  fig, axes = plt.subplots(2, 2, figsize=(7,7), subplot_kw=dict(rasterized=True))
  fig.tight_layout(pad=4, h_pad=4, w_pad=4)
  for (audio, family) in draw_data:
    x = np.linspace(0, len(audio) / 16000, num=len(audio))
    axes[i // 2][i % 2].plot(x, audio, "|")
    axes[i // 2][i % 2].set_title(families[family].capitalize())
    axes[i // 2][i % 2].set_ylim(-1,1)
    axes[i // 2][i % 2].set_ylabel("Audio sample")
    axes[i // 2][i % 2].set_xlabel("Time (s)")
    i += 1
  fig.suptitle("Audio data")

  plt.savefig('figures/example-audio.pgf', dpi=1000)

  i = 0
  fig, axes = plt.subplots(2, 2, figsize=(7,7), subplot_kw=dict(rasterized=True))
  fig.tight_layout(pad=4, h_pad=4, w_pad=4)
  for (audio, family) in draw_data:
    spectrogram, family = preproccess_spectrogram(audio, family)
    plot_spectrogram(spectrogram, axes[i // 2][i % 2])
    axes[i // 2][i % 2].set_title(families[family].capitalize())
    axes[i // 2][i % 2].axes.xaxis.set_ticks([])
    axes[i // 2][i % 2].axes.yaxis.set_ticks([])
    axes[i // 2][i % 2].set_xlabel("Time")
    axes[i // 2][i % 2].set_ylabel("Frequency")
    i += 1
  fig.suptitle("Spectrograms")

  plt.savefig('figures/example-spectrogram.pgf', dpi=1000)

get_data(DATA_PATH, "train.tfrecord",
  "http://download.magenta.tensorflow.org/datasets/nsynth/nsynth-train.tfrecord")
get_data(DATA_PATH, "valid.tfrecord",
  "http://download.magenta.tensorflow.org/datasets/nsynth/nsynth-valid.tfrecord")
get_data(DATA_PATH, "test.tfrecord",
  "http://download.magenta.tensorflow.org/datasets/nsynth/nsynth-test.tfrecord")

# use test data for now as it is much smaller so shuffle takes less time
train_data = tf.data.TFRecordDataset("data/nsynth/valid.tfrecord")
val_data = tf.data.TFRecordDataset("data/nsynth/test.tfrecord")
test_data = tf.data.TFRecordDataset("data/nsynth/test.tfrecord")

test_data = test_data.shuffle(buffer_size=3000)
train_data = train_data.shuffle(buffer_size=10000)
val_data = val_data.shuffle(buffer_size=3000)

train_data = train_data.map(parse_test, num_parallel_calls=AUTOTUNE)
val_data = val_data.map(parse_test, num_parallel_calls=AUTOTUNE)
test_data = test_data.map(parse_test, num_parallel_calls=AUTOTUNE)

plot_data_examples()

train_data = train_data.map(preproccess_spectrogram, num_parallel_calls=AUTOTUNE)
val_data = val_data.map(preproccess_spectrogram, num_parallel_calls=AUTOTUNE)
test_data = test_data.map(preproccess_spectrogram, num_parallel_calls=AUTOTUNE)

for (spectrogram, family) in train_data.take(1):
  input_shape = spectrogram.shape
num_labels = len(families)

train_data = train_data.batch(BATCH_SIZE)
val_data = val_data.batch(BATCH_SIZE)

train_data = train_data.cache().prefetch(AUTOTUNE)
val_data = val_data.cache().prefetch(AUTOTUNE)

model = models.Sequential([
    layers.Input(shape=input_shape),
    layers.Resizing(32, 32),
    layers.Conv2D(32, 3, activation='relu'),
    layers.Conv2D(64, 3, activation='relu'),
    layers.MaxPooling2D(),
    layers.Dropout(0.25),
    layers.Flatten(),
    layers.Dense(128, activation='relu'),
    layers.Dropout(0.5),
    layers.Dense(num_labels),
])

model.summary()

model.compile(
    optimizer=tf.keras.optimizers.Adam(),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=['accuracy'],
)

history = model.fit(
    train_data,
    validation_data=val_data,
    epochs=EPOCHS,
    callbacks=tf.keras.callbacks.EarlyStopping(verbose=1, patience=2),
    verbose=2
)

plt.figure(figsize=(4,4))
metrics = history.history
plt.plot(history.epoch, metrics['loss'], metrics['val_loss'])
plt.legend(['Training loss', 'Validation loss'])
plt.title('Training and validation loss against epoch')
plt.xlabel('Epoch')
plt.ylabel("Loss (categorical crossentropy)")

plt.savefig('figures/epoch-loss.pgf')

test_spectrogram = []
test_family = []

for spectrogram, family in test_data:
  test_spectrogram.append(spectrogram.numpy())
  test_family.append(family.numpy())

test_spectrogram = np.array(test_spectrogram)
test_family = np.array(test_family)

y_pred = np.argmax(model.predict(test_spectrogram), axis=1)
y_true = test_family

test_acc = sum(y_pred == y_true) / len(y_true)
print(f'Test set accuracy: {test_acc:.0%}')

confusion_mtx = tf.math.confusion_matrix(y_true, y_pred)
plt.figure(figsize=(7.8, 7))
plt.tight_layout(pad=5, rect=(0,0,1,1))
sns.heatmap(confusion_mtx,
            xticklabels=families,
            yticklabels=families,
            annot=True, fmt='g')
label = plt.xlabel('Prediction')
label.set_color("black")
plt.ylabel('Label')
plt.xticks(rotation = 25)
plt.title("Model predictions and actual label for all test data")

plt.savefig('figures/heatmap.pgf')

test_data = test_data.batch(1)

for spectrogram, family in test_data.take(1):
  plt.figure(figsize=(7,4.5))
  plt.tight_layout(pad=4, rect=(0,0,1,1))
  prediction = model(spectrogram)
  plt.bar([x.capitalize() for x in families], tf.nn.softmax(prediction[0]))
  plt.title(f'Prediction for {families[family[0]].capitalize()}')
  plt.xticks(rotation = 25)
  plt.ylabel("Confidence")


plt.savefig('figures/example-predictions.pgf')

plt.show()