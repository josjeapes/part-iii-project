directory="."

while getopts d: flag
do
	case "${flag}" in
		d) directory=${OPTARG};;
	esac
done

curl http://download.magenta.tensorflow.org/datasets/nsynth/nsynth-train.tfrecord > "$directory/NSynth/train.tfrecord"
curl http://download.magenta.tensorflow.org/datasets/nsynth/nsynth-valid.tfrecord > "$directory/NSynth/valid.tfrecord"
curl http://download.magenta.tensorflow.org/datasets/nsynth/nsynth-test.tfrecord > "$directory/NSynth/test.tfrecord"